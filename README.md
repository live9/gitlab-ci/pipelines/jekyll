# Jekyll Pipeline

Pipelines designed to be included with the aim to centralize maintenance while using them for multiple Jekyll sites.

Read the [notes](#notes) for more detailed information.

## Usage

1. Create a Jekyll project in GitLab ensurign the root directory contains the Jekyll files structure (ex: gitlab.com/gl-group/jekyll/project)
2. Add a `.gitlab_ci.yml` file with this content (for now do not change vars value) to include the **build** pipeline:

```
---
include:
  - project: 'live9/gitlab-ci/pipelines/jekyll'
    ref: main
    file: 'build_gitlab_ci.yml'

variables:
  ### Build ###
  RUBY_VERSION:
    description: "Reference: https://hub.docker.com/_/ruby/tags"
    value: "2.7.0"
  BUNDLER_VERSION:
    description: "Reference: run `gem list ^bundler$ --remote --all`"
    value: "1.13.6"
  ALPINE_VERSION:
    description: "Reference: https://hub.docker.com/_/alpine/tags"
    value: "3"
  ### Deploy ###
  CURL_VERSION:
    description: "Reference: https://hub.docker.com/r/curlimages/curl/tags"
    value: "8.00.1"
  ARTIFACT_URL:
    description: "Gitlab page url + filename"
    value: "https://project.example.org/$CI_COMMIT_SHORT_SHA.tgz"
  FEA_PROJECT:
    description: "Project used to deploy feature branch as gitlab page"
    value: "gl-group/deploy-pages/project-development"
  PREPROD_PROJECT:
    description: "Project used to deploy preproduction as gitlab page"
    value: "gl-group/deploy-pages/project-staging"
  PROD_PROJECT:
    description: "Project used to deploy production as gitlab page"
    value: "gl-group/deploy-pages/project-production"

merge_error:
  tags:
  - docker
  # add customized tags from here
build:
  tags:
  - docker
  # add customized tags from here
pages:
  tags:
  - docker
  # add customized tags from here
...
```

3. If you use your own runners modify the tags at the end of the file.
4. Check if the build stage in the pipeline works. If it does not change `RUBY_VERSION` and `BUNDLER_VERSION` vars until you find a pair that works for your project.
5. Configure the Pages for this project because we will use it to deploy the artifact (read [notes](#notes))
   - ex: https://build-project.example.org
   - set  `ARTIFACT_URL` var
6. Per environment create a Jekyll project in GitLab and set `FEA_PROJECT`, `PREPROD_PROJECT` and `PROD_PROJECT` var values.
7. Per environment project add a `.gitlab_ci.yml` file with this content (feel free to change vars value) to include the **deploy** pipeline:

```
---
include:
  - project: 'live9/gitlab-ci/pipelines/jekyll'
    ref: main
    file: 'deploy_gitlab_ci.yml'

pages:
  tags:
  - docker
  # add customized tags from here
...
```

## Authors and acknowledgment

Originally writed for Fundación Karisma internal usage. Released here to made this work available to others.


## Notes

CI_JOB_TOKEN can not be used on Gitlab SaaS free tier to pull artifacts from pipelines in other projects then we we made public the artifact that contains the static website in the gitlab page of the project.

We asume you will always work under a main gitlab group (ex: gitlab.com/gl-group)


## License

GPL v3
